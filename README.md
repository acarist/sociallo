Sociallo Test Project
===================

Sociallo project is test for social login with django. Google&Facebook login don't work with local environment.  You can test twitter login on the local environment. All api key is ready for testing ( attached to my social media accounts).

Installation
-------------

Installation has very basic steps :

Clone Project 
```
$ git clone https://acarist@bitbucket.org/acarist/sociallo.git
```
Go to folder 
```
$ cd sociallo
```
Create virtual environment (optional)
```
$ virtualenv .
$ source bin/activate
//you will see this
(socaillo)->$
```
Install requirements

```
(socaillo)->$ pip install -r req.txt
```

Run migration
```
(socaillo)->$ python manage.py migrate
```


Finally serve project
```
(socaillo)->$ python manage.py runserver
```

Thats it!
